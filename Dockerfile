FROM python:buster
RUN mkdir /app
RUN apt update
RUN apt install -y nginx
COPY requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt
COPY app.py /app/app.py
WORKDIR /app/
ENTRYPOINT ["uwsgi", "--http-socket", ":8083", "--gevent", "1000", "--http-websockets", "--master", "--wsgi-file", "/app/app.py", "--callable", "app"]