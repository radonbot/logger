from flask import Flask, Response, render_template, request
from flask_socketio import SocketIO
from bson import json_util
import pymongo
import json

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
sio = SocketIO(app)

with open("/app/config.json", "r") as f:
    CFG = json.read(f)

db_client = pymongo.MongoClient(CFG["connectionString"])["discord_bot"]
log = db_client["log"]


@app.route("/index.html")
def index():
    return render_template("/index.html")


def is_authed(request):
    apikey = request.headers.get("Authorization")
    if(apikey == "MinFetaBalle2014!"):
        return True
    return False


@app.route("/api/v1/log/<event_id>", methods=["PUT", "POST"])
def add_event_to_log(event_id):
    """Create a new event logging "thread"."""
    req = request.json
    if not is_authed(request):
        status = "FAIL"
        msg = "Invalid Authorization header"
        return Response(
            json_util.dumps({"status": status, "msg": msg})
        )
    if request.method == "POST":
        return log_to_event(event_id, req)
    

    status = "SUCCESS"
    msg = f"Log event {event_id} successfully created."
    try:
        doc = {
            "type": "event_start",
            "event_id": event_id,
            "user": req["user_id"],
            "guild": req["guild_id"],
            "message": req["message"],
            "dispatcher": req["dispatcher"],
            "execution_start": req["execution_start"]
        }
    except NameError as e:
        status = "FAIL"
        msg = f"Missing key {e} in body."

    log.insert_one(doc)
    status = "SUCCESS"
    return Response(
        json_util.dumps({"status": status, "msg": msg})
    )


def log_to_event(event_id):
    req = request.json
    status = "SUCCESS"
    msg = f"Logged to {event_id}"
    try:
        doc = {
            "type": "entry",
            "message": req["message"],
            "dispatcher": req["dispatcher"],
            "time": req["time"],
            "event_id": event_id
        }
    except NameError as e:
        status = "FAIL"
        msg = f"Missing key {e} in body."

    log.insert_one(doc)
    return Response(
        json_util.dumps({"status": status, "msg": msg})
    )


@app.route("/api/v1/log/<event_id>/end", methods=["PUT"])
def log_to_event(event_id):
    if not is_authed(request):
        status = "FAIL"
        msg = "Invalid Authorization header"
        return Response(
            json_util.dumps({"status": status, "msg": msg})
        )
    req = request.json
    status = "SUCCESS"
    msg = f"Logged to {event_id}"
    try:
        doc = {
            "type": "event_end",
            "message": req["message"],
            "dispatcher": req["dispatcher"],
            "execution_end": req["execution_end"],
            "event_id": event_id,
            "status": req["status"]
        }
    except NameError as e:
        status = "FAIL"
        msg = f"Missing key {e} in body."

    log.insert_one(doc)
    return Response(
        json_util.dumps({"status": status, "msg": msg})
    )


@sio.on("register", namespace="/logger")
def socket_event_register(json):
    """Register the client and return last 5 event triggers and entries so far.
    """
    sio.send({"log": log}, json=True)


if __name__ == "__main__":
    app.run(host="0.0.0.0")
